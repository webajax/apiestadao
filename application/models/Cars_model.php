<?php


#Created André 26/05/2019


class Cars_model extends CI_Model {
   
    protected $path;

    function __construct() {
		#$this->load->database();
      #file bd  
      $this->path = "./bd/cars.txt";  
    }
   

    public function new($data){

      #add BD

      try{        



      #------------------------------------------add
      $id     = $data["id"];
      $marca  = $data["marca"];
      $modelo = $data["modelo"];
      $ano    = $data["ano"];
      
      
      $f=fopen($this->path,"a+",0);
      
      $respLines = $this->countLinesTXT();


      #quebra line
      $quebra = chr(13).chr(10);

 

      $line = $id .";".$marca.";".$modelo.";". $ano.";\n" ;


      $resp = fwrite($f,$quebra.$line,strlen($line));
      fclose($f);

      $respLinesafter =  $this->countLinesTXT();


      if($respLines < $respLinesafter){

        #----------------------------------------SUCCESS!
        $query = array(
            'query'    => true,
            'exception'=> null,
            'idnow'    => $id
        );

      }else{

        #----------------------------------------FALSE ADD REG
        $query = array(
            'query'    => false,
            'exception'=> "Registro não adicionado!"
        );
      }
     


        return $query;
    
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;

      }


    }

    public function delete($id){

  

      #------------------------------------------Open file txt
              $pointer = fopen ($this->path,"r");
              $text = "";

              $respLines =  $this->countLinesTXT();


               
                #--------------------------------------------------read file txt
                while (!feof ($pointer)) {

                    $line = fgets($pointer,4096);#ready line

                    $col = explode(";", $line , 4);#col 6 merge cols after
       


                    if(isset($col[1]) ) {

                       if($col[0]!=$id and $col[0]!="" ){
                            $text = $text . $col[0].";".$col[1].";".$col[2].";".$col[3].";". "\n";
                       }
                    
                    }

                } 



                 unlink($this->path);
                 $pointer = fopen($this->path, "w");
                 chmod($this->path, 0777);

                #broken line
                $quebra = chr(13).chr(10);

                $resp = fwrite($pointer,$text,strlen($text));
                fclose($pointer);

                $respLinesafter =  $this->countLinesTXT();


                 if($respLines != $respLinesafter){

                    #----------------------------------------SUCCESS!
                    $query = array(
                        'query'    => true,
                        'exception'=> null,
                    );

                 }else{

                    #----------------------------------------FALSE ADD REG
                    $query = array(
                        'query'    => false,
                        'exception'=> "Opsss!Registro não foi excluído!"
                    );
                 }  

                return $query; 



    }

    public function list(){

      try{        

      #------------------------------------------Open file txt
      $pointer = fopen ($this->path,"r");
      $data = array();
      $query = null;

        #--------------------------------------------------read file txt
        while (!feof ($pointer)) {

            $line = fgets($pointer,4096);#ready line

            $col = explode(";", $line , 6);#col 6 merge cols after



            if(isset($col[1])){

                if($col[0]!=null or $col[0]!=""){

                  $data["list"][] = array(

                      "id"     => $col[0],
                      "marca"  => $col[1],
                      "modelo" => $col[2],
                      "ano"    => $col[3],
                  );


                  #----------------------------------------add  result list array and total lines
                  $query = array(
                      'query'    => $data,
                      'count'    => count($data["list"]),
                      'exception'=> null
                  );              
                }  

           }
          
        }  


        return $query;

    
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;

      }

    }


    public function find($id){

      try{        

      #------------------------------------------Open file txt
      $pointer = fopen ($this->path,"r");
      $data = array();
      $query = null;

        #--------------------------------------------------read file txt
        while (!feof ($pointer)) {

            $line = fgets($pointer,4096);#ready line

            $col = explode(";", $line , 4);#col 6 merge cols after

                 

            if(isset($col[1])){

                if($col[0]==$id ){


                  $data["list"][] = array(

                      "id"     => $col[0],
                      "marca"  => $col[1],
                      "modelo" => $col[2],
                      "ano"    => $col[3],
                  );


                  #----------------------------------------add  result list array and total lines
                  $query = array(
                      'query'    => $data,
                      'count'    => count($data["list"]),
                      'exception'=> null
                  );  

                            
                }  

           }
          
        }  


        return $query;

    
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;

      }

    }

    public function update($data){


      #------------------------------------------Open file txt
        $pointer = fopen ($path,"r");
        $text = "";

        $respLines =  $this->countLinesTXT();
         
          #--------------------------------------------------read file txt
          while (!feof ($pointer)) {

              $line = fgets($pointer,4096);#ready line

              $col = explode(";", $line , 4);#col 6 merge cols after
 


              if(isset($col[1]) ) {

                 if($col[0]!=$data['id'] and $col[0]!="" ){
                      $text = $text . $col[0].";".$data['marca'].";".$data['modelo'].";".$data['ano'].";". "\n";
                 }
              
              }

          } 

           unlink($path);
           $pointer = fopen($path, "w");
           chmod($path, 0777);

          #broken line
          $quebra = chr(13).chr(10);

          $resp = fwrite($pointer,$text,strlen($text));
          fclose($pointer);

          $respLinesafter =  $this->countLinesTXT();


           if($respLines != $respLinesafter){

              #----------------------------------------SUCCESS!
              $query = array(
                  'query'    => true,
                  'exception'=> null,
              );

           }else{

              #----------------------------------------FALSE ADD REG
              $query = array(
                  'query'    => false,
                  'exception'=> "Opsss!Registro não foi excluído!"
              );
           }  

          return $query; 

    }


    public function lastReg(){


       try{        

            #get last line
            $line=exec('tail -n 1 ' .$this->path);


            $col = explode(";", $line , 4);#col 6 merge cols after



            $query = array(
               'query'    => $col[0],
               
            );

            return $query;
          
            }catch(Exception $e){

              $errorInfo = array(
                  'query'    => 0,
                  'exception'=>$e->getMessage
              );

                  return $errorInfo;

            }

    }


    public function countLinesTXT(){

        #------------------------------------------COUNT total lines txt

            #Eu utilizo o PHP_INT_MAX para apontar para a última linha do arquivo, pois SplFileObject implementa SeekableIterator.
            #Daí, como a contagem das linhas começa por 0, eu tenho que somar +1 para trazer o valor certo.
            $file = new \SplFileObject($this->path, 'r');
            $file->seek(PHP_INT_MAX);
            $totallines = $file->key();
        #------------------------------------------End COUNT

            return $totallines;

    }

}