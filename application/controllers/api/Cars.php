<?php
   

require(APPPATH.'libraries/REST_Controller.php');
header( "Access-Control-Allow-Origin: *" );
header( "Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS,DELETE" );
header( "Access-Control-Allow-Headers: merchantId, merchantKey, content-type" );
header( "Content-Type: application/json");


class Cars extends REST_Controller {

    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->model("cars_model");
       $this->load->helper('url');

    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function list_get()
	{
        
        #get list cars
        $response = $this->cars_model->list();



        if($response["count"] >0 ){

            #result
            $data["success"] = TRUE;
            $data["list"]    = array_filter($response["query"]["list"]);
            $data["count"]   = count($data["list"]);

        }else{

            #result
            $data["success"] = false;
            $data            = null;
            $data["count"]   = 0;

        }

        if(isset($data["list"])){
            #get last insert
            $this->key = end($data["list"]);
            $this->key = $this->key["id"];
        }    


       #return  $this->response($data, REST_Controller::HTTP_OK);
        echo json_encode($data, 200);
        
	}


    /**
     * Get one Data from this method.
     *
     * @return Response
    */
    public function find_get($id)
    {

        #get list cars
        $response = $this->cars_model->find($id);

        

        if($response["count"] >0 ){

            #result
            $data["success"]  = true;
            $data["list"]     = array_filter($response["query"]["list"]);
            $data["count"]    = count($data["list"]);

        }else{

            #result
            $data["success"]  = false;
            $data             = null;
            $data["count"]    = 0;

        }

        if(isset($data["list"])){
            #get last insert
            $this->key = end($data["list"]);
            $this->key = $this->key["id"];
        }    



       #return  $this->response($data, REST_Controller::HTTP_OK);
        echo json_encode($data);
        
    }

        /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function new_post()
    {

        $id     = $this->input->post("id");
        $marca  = $this->input->post("marca");
        $modelo = $this->input->post("modelo");
        $ano    = $this->input->post("ano");

        #post register txt
        $data = array(
        
            'id'     => $id,
            'marca'  => $marca,
            'modelo' => $modelo,
            'ano'    => $ano,
        );

        #response add
        $response = $this->cars_model->new($data);

        #if true
        if($response["query"]){

            $data = array(

                "success"  => true,
                "message"  => "Cadastrado com sucesso!",
                "response" => $data,
                "idnow"    => $response["idnow"]

            );

        
        }else{

            $data = array(

                "success" => false,
                "message" => $data["exception"]

            );

        }


        echo json_encode($data);


    } 


    public function update_put($data)
        {


            #get last register txt
            $response = $this->cars_model->update($data);
            $data=null;

            #if true
            if($response["query"]){

                $data = array(

                    "success"  => true,
                    "message"  => "Editado com sucesso!",
                );

            
            }else{

                $data = array(

                    "success" => false,
                    "message" => $data["exception"]

                );

            }


            echo json_encode($data);        

        } 
    


    public function delete_delete($id)
        {
            

            #get last register txt
            $response = $this->cars_model->delete($id);
            $data=null;

            #if true
            if($response["query"]){

                $data = array(

                    "success"  => true,
                    "message"  => "Deletado com sucesso!",
                );

            
            }else{

                $data = array(

                    "success" => false,
                    "message" => $data["exception"]

                );

            }


            echo json_encode($data);        


        } 


    public function lastReg_get(){


        #get last register txt
        $response = $this->cars_model->lastReg();

        echo json_encode($response);


    }

    public function index_options(){}
    public function index_get(){}

}
